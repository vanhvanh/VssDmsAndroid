package com.vss.dmsvss.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.navigation.NavigationView;
import com.vss.dmsvss.R;
import com.vss.dmsvss.base.BaseActivity;
import com.vss.dmsvss.databinding.ActivityMainBinding;
import com.vss.dmsvss.ui.fragment.home.HomeFragment;

import javax.inject.Inject;

public class MainActivity extends BaseActivity<ActivityMainBinding> {

    @Override
    protected ActivityMainBinding getLayoutBinding() {
        return ActivityMainBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void updateUI(@Nullable Bundle savedInstanceState) {
        initView();
        initEvent();
    }

    private void initEvent() {
        binding.ivMenuHamburger.setOnClickListener(view ->
                binding.drawerLayoutMain.openDrawer(GravityCompat.END));

        binding.navMain.setNavigationItemSelectedListener(item -> {
            binding.drawerLayoutMain.closeDrawers();
            switch (item.getItemId()) {
                case R.id.navListRoute:
                    toast(getString(R.string.str_route_list));
                    break;
                case R.id.navCustomer:
                    toast(getString(R.string.str_customer));
                    break;
                case R.id.navProduct:
                    toast(getString(R.string.str_product));
                    break;
                case R.id.navOrder:
                    toast(getString(R.string.str_order));
                    break;
                case R.id.navReport:
                    toast(getString(R.string.str_report));
                    break;

            }
            return false;
        });
    }

    private void initView() {
        openFragment(binding.frMain.getId(), HomeFragment.class, null, true);
    }
}