package com.vss.dmsvss.ui.fragment.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vss.dmsvss.data.model.ModuleHome;
import com.vss.dmsvss.databinding.ItemModuleHomeBinding;

import java.util.List;

public class AdapterHome extends RecyclerView.Adapter<AdapterHome.ViewHolder> {
    private List<ModuleHome> list;

    public AdapterHome(List<ModuleHome> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public AdapterHome.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemModuleHomeBinding
                .inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterHome.ViewHolder holder, int position) {
        ModuleHome item = list.get(position);
        holder.binding.ivModuleHome.setImageResource(item.getSrc());
        holder.binding.tvModuleHome.setText(item.getTitle());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ItemModuleHomeBinding binding;

        public ViewHolder(@NonNull ItemModuleHomeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
