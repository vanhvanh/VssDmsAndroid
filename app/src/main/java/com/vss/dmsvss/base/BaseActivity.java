package com.vss.dmsvss.base;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewbinding.ViewBinding;

public abstract class BaseActivity<T extends ViewBinding> extends AppCompatActivity {
    protected T binding;

    protected abstract T getLayoutBinding();

    protected abstract void updateUI(@Nullable Bundle savedInstanceState);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getLayoutBinding();
        setContentView(binding.getRoot());
        updateUI(savedInstanceState);
    }

    protected void openFragment(int resId, Class fragmentClazz, Bundle args, boolean addBackstack) {
        String tag = fragmentClazz.getSimpleName();
        try {
            boolean isExisted = getSupportFragmentManager().popBackStackImmediate(tag, 0);
            if (!isExisted) {
                Fragment fragment;
                try {
                    fragment = (Fragment) (fragmentClazz.asSubclass(Fragment.class)).newInstance();
                    fragment.setArguments(args);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.add(resId, fragment, tag);
                    if (addBackstack) {
                        transaction.addToBackStack(tag);
                    }
                    transaction.commitAllowingStateLoss();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
