package com.vss.dmsvss;

import android.app.Application;
import android.util.Log;

import com.vss.dmsvss.di.component.AppComponent;
import com.vss.dmsvss.di.component.DaggerAppComponent;

import javax.inject.Inject;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class VssApplication extends Application {
    public AppComponent appComponent;

    @Inject
    CalligraphyConfig calligraphyConfig;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .application(this)
                .build();

        appComponent.inject(this);

        CalligraphyConfig.initDefault(calligraphyConfig);
        Log.e("vanh",calligraphyConfig.toString());
    }
}
